from flask_restful import Resource, Api
from flask import Blueprint, jsonify

rest_bp = Blueprint("rest_bp", __name__)
api = Api(rest_bp)


class crudUser(Resource):
    def get(self):
        return jsonify({"get": "ok"})

    def post(self):
        return jsonify({"post": "ok"})


api.add_resource(crudUser, "/user")

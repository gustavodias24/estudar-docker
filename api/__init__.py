from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

from blueprints.rest_bp import rest_bp

app.register_blueprint(rest_bp)

if __name__ == "__main__":
    app.run(port=8181)

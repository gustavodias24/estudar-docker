FROM python
WORKDIR /usr/src/app
COPY requeriments.txt requeriments.txt
COPY . .
RUN pip install --no-cache-dir -r requeriments.txt && apt update && apt clean
CMD ["gunicorn","-w", "8" ,"-p","8181", "api:app"]